package com.splttr.applicationonline.person;

import com.splttr.applicationservice.person.PersonApplicationService;
import com.splttr.security.PrincipalService;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoRule;

import static com.google.common.collect.Lists.newArrayList;
import static com.splttr.testconstants.ApplicationTestConstants.PRINCIPAL;
import static com.splttr.testconstants.person.PersonTestConstants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.junit.MockitoJUnit.rule;

public class PersonControllerTest {

    @Rule
    public MockitoRule mockitoRule = rule();

    @Mock
    private PrincipalService principalService;

    @Mock
    private PersonApplicationService personApplicationService;

    @InjectMocks
    private PersonController personController;

    @Test
    public void getAllPersons() {
        when(principalService.getPrincipalId()).thenReturn(PRINCIPAL);
        when(personApplicationService.getAllUserPersons(PRINCIPAL)).thenReturn(newArrayList(personTO()));

        assertThat(personController.getAllPersons()).containsExactly(personTO());
    }

    @Test
    public void createPerson() {
        when(principalService.getPrincipalId()).thenReturn(PRINCIPAL);

        personController.createPerson(personInfoTO());

        verify(personApplicationService).createPerson(personInfoTO(), PRINCIPAL);
    }

    @Test
    public void editPerson() {
        when(principalService.getPrincipalId()).thenReturn(PRINCIPAL);

        personController.editPerson(PERSON_ID_VALUE, personInfoTO());

        verify(personApplicationService).editPerson(PERSON_ID_VALUE, personInfoTO(), PRINCIPAL);
    }

}
