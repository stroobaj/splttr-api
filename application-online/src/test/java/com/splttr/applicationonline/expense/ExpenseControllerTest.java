package com.splttr.applicationonline.expense;

import com.splttr.applicationservice.expense.ExpenseApplicationService;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoRule;

import static com.splttr.testconstants.expense.ExpenseTestConstants.createExpenseTO;
import static org.mockito.Mockito.verify;
import static org.mockito.junit.MockitoJUnit.rule;

public class ExpenseControllerTest {

    @Rule
    public MockitoRule mockitoRule = rule();

    @Mock
    private ExpenseApplicationService expenseApplicationService;

    @InjectMocks
    private ExpenseController expenseController;

    @Test
    public void createExpense() {
        expenseController.createExpense(createExpenseTO());

        verify(expenseApplicationService).createExpense(createExpenseTO());
    }

}
