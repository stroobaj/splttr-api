package com.splttr.applicationonline.balance;

import com.splttr.applicationservice.balance.BalanceApplicationService;
import com.splttr.security.PrincipalService;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoRule;

import static com.google.common.collect.Lists.newArrayList;
import static com.splttr.testconstants.ApplicationTestConstants.PRINCIPAL;
import static com.splttr.testconstants.balance.BalanceTestConstants.personBalanceTO;
import static com.splttr.testconstants.balance.BalanceTestConstants.userBalanceTO;
import static com.splttr.testconstants.trip.TripTestConstants.TRIP_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.junit.MockitoJUnit.rule;

public class BalanceControllerTest {

    @Rule
    public MockitoRule mockitoRule = rule();

    @Mock
    private BalanceApplicationService balanceApplicationService;

    @Mock
    private PrincipalService principalService;

    @InjectMocks
    private BalanceController balanceController;

    @Test
    public void getTripBalances() {
        when(balanceApplicationService.getTripBalances(TRIP_ID)).thenReturn(newArrayList(personBalanceTO()));

        assertThat(balanceController.getTripBalances(TRIP_ID)).containsExactly(personBalanceTO());
    }

    @Test
    public void getUserBalances() {
        when(principalService.getPrincipalId()).thenReturn(PRINCIPAL);
        when(balanceApplicationService.getUserBalances(PRINCIPAL)).thenReturn(newArrayList(userBalanceTO()));

        assertThat(balanceController.getUserBalances()).containsExactly(userBalanceTO());
    }

    @Test
    public void getUserBalancesOnLastEditedTrips() {
        when(principalService.getPrincipalId()).thenReturn(PRINCIPAL);
        when(balanceApplicationService.getLastEditedUserBalances(PRINCIPAL)).thenReturn(newArrayList(userBalanceTO()));

        assertThat(balanceController.getUserBalancesOnLastEditedTrips()).containsExactly(userBalanceTO());
    }
}
