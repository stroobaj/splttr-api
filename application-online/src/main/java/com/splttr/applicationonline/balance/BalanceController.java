package com.splttr.applicationonline.balance;

import com.splttr.applicationapi.balance.PersonBalanceTO;
import com.splttr.applicationapi.balance.UserBalanceTO;
import com.splttr.applicationservice.balance.BalanceApplicationService;
import com.splttr.security.PrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/balance")
public class BalanceController {

    private final BalanceApplicationService balanceApplicationService;
    private final PrincipalService principalService;

    @Autowired
    public BalanceController(BalanceApplicationService balanceApplicationService, PrincipalService principalService) {
        this.balanceApplicationService = balanceApplicationService;
        this.principalService = principalService;
    }

    @GetMapping(path = "/{tripId}", produces = "application/json")
    public List<PersonBalanceTO> getTripBalances(@PathVariable UUID tripId) {
        return balanceApplicationService.getTripBalances(tripId);
    }

    @GetMapping(produces = "application/json")
    public List<UserBalanceTO> getUserBalances() {
        return balanceApplicationService.getUserBalances(principalService.getPrincipalId());
    }

    @GetMapping(path = "/recent", produces = "application/json")
    public List<UserBalanceTO> getUserBalancesOnLastEditedTrips() {
        return balanceApplicationService.getLastEditedUserBalances(principalService.getPrincipalId());
    }
}
