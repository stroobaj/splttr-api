package com.splttr.applicationonline.person;

import com.splttr.applicationapi.person.PersonInfoTO;
import com.splttr.applicationapi.person.PersonTO;
import com.splttr.applicationservice.person.PersonApplicationService;
import com.splttr.security.PrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/person")
public class PersonController {

    @Autowired
    private PersonApplicationService personApplicationService;

    @Autowired
    private PrincipalService principalService;

    @GetMapping(produces = "application/json")
    public List<PersonTO> getAllPersons() {
        return personApplicationService.getAllUserPersons(principalService.getPrincipalId());
    }

    @PostMapping(consumes = "application/json")
    public void createPerson(@RequestBody @Valid PersonInfoTO personInfoTO) {
        personApplicationService.createPerson(personInfoTO, principalService.getPrincipalId());
    }

    @PostMapping(path = "/{personId}/edit", consumes = "application/json")
    public void editPerson(@PathVariable String personId,
                           @RequestBody @Valid PersonInfoTO personInfoTO) {
        personApplicationService.editPerson(personId, personInfoTO, principalService.getPrincipalId());
    }

}
