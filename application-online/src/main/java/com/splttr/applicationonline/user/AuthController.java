package com.splttr.applicationonline.user;

import com.splttr.applicationapi.user.LoginCredentials;
import com.splttr.applicationapi.user.UserDataTO;
import com.splttr.applicationservice.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth")
public class AuthController {

    @Autowired
    private UserService userService;

//    @Autowired
//    private AuthenticationManager authenticationManager;
//
//    @Autowired
//    private ModelMapper modelMapper;

    @PostMapping(path = "/signup", consumes = "application/json")
    public ResponseEntity<?> signup(@RequestBody UserDataTO user) {
        return userService.signup(user);
    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> login(@RequestBody LoginCredentials loginCredentials) {
        return userService.login(loginCredentials);
    }


//    @DeleteMapping(value = "/{username}")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    public String delete(@PathVariable String username) {
//        userService.delete(username);
//        return username;
//    }
//
//    @GetMapping(value = "/{username}")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    public UserResponseTO search(@PathVariable String username) {
//        return modelMapper.map(userService.search(username), UserResponseTO.class);
//    }
//
//    @GetMapping(value = "/me")
//    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
//    public UserResponseTO whoami(HttpServletRequest req) {
//        return modelMapper.map(userService.whoami(req), UserResponseTO.class);
//    }

}
