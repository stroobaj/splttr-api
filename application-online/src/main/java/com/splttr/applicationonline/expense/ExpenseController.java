package com.splttr.applicationonline.expense;

import com.splttr.applicationapi.expense.CreateExpenseTO;
import com.splttr.applicationservice.expense.ExpenseApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/expense")
public class ExpenseController {

    @Autowired
    private ExpenseApplicationService expenseApplicationService;

    @PostMapping(consumes = "application/json")
    public void createExpense(@RequestBody CreateExpenseTO createExpenseTO) {
        expenseApplicationService.createExpense(createExpenseTO);
    }

}
