package com.splttr.applicationonline;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.splttr.commons.CommonsConfiguration;
import com.splttr.infrastructure.SplttrInfrastructureConfiguration;
import com.splttr.security.SecurityConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

@Configuration
@ComponentScan(basePackages = {"com.splttr.applicationservice", "com.splttr.domain"})
//@EnableJpaRepositories(basePackages = {"com.splttr.domain"})
@Import({SplttrInfrastructureConfiguration.class, SecurityConfiguration.class, CommonsConfiguration.class})
public class ApplicationOnlineConfiguration {

    @Bean
    public ObjectMapper objectMapper(List<Module> allModuleBeans) {
        return new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .findAndRegisterModules()
                .registerModules(allModuleBeans)
                .registerModule(new Jdk8Module())
                .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
                .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
                .setSerializationInclusion(NON_EMPTY)
                .setSerializationInclusion(NON_NULL)
                .disable(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE)
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
                .disable(WRITE_DATES_AS_TIMESTAMPS);
    }

}
