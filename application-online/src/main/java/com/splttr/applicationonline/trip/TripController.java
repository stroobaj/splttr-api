package com.splttr.applicationonline.trip;

import com.splttr.applicationapi.expense.ExtendedExpenseInfoTO;
import com.splttr.applicationapi.trip.CreateTripTO;
import com.splttr.applicationapi.trip.TripOverviewTO;
import com.splttr.applicationservice.expense.ExpenseApplicationService;
import com.splttr.applicationservice.trip.TripApplicationService;
import com.splttr.security.PrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/trip")
public class TripController {

    @Autowired
    private TripApplicationService tripApplicationService;

    @Autowired
    private ExpenseApplicationService expenseApplicationService;

    @Autowired
    private PrincipalService principalService;

    @GetMapping(produces = "application/json")
    public List<TripOverviewTO> getAllUserTrips() {
        return tripApplicationService.getAllUserTrips(principalService.getPrincipalId());
    }

    @GetMapping(path = "/{tripId}", produces = "application/json")
    public TripOverviewTO getUserTripById(@PathVariable UUID tripId) {
        return tripApplicationService.getUserTripById(tripId, principalService.getPrincipalId());
    }

    @GetMapping(path = "/{tripId}/expense", produces = "application/json")
    public List<ExtendedExpenseInfoTO> getExtendExpenseInfoByTripId(@PathVariable UUID tripId) {
        return expenseApplicationService.getExtendExpensesByByTripId(tripId);
    }

    @PostMapping(consumes = "application/json")
    public void createUserTrip(@RequestBody CreateTripTO createTripTO) {
        tripApplicationService.createUserTrip(createTripTO, principalService.getPrincipalId());
    }

}
