package com.splttr.testconstants.expense;

import com.google.common.collect.Lists;
import com.splttr.applicationapi.expense.CreateExpenseTO;
import com.splttr.domain.expense.Expense;
import com.splttr.domain.expense.ExpenseType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static com.splttr.testconstants.person.PersonTestConstants.PERSON_ID;
import static com.splttr.testconstants.trip.TripTestConstants.TRIP_ID;

public class ExpenseTestConstants {

    public static final UUID EXPENSE_ID = UUID.fromString("d1a38ea5-de83-44ba-9102-a384558417d9");
    public static final String EXPENSE_DESCRIPTION = "Expense description.";
    public static final LocalDate EXPENSE_DATE = LocalDate.of(2019, 1, 1);
    public static final String EXPENSE_DATE_AS_STRING = EXPENSE_DATE.toString();
    public static final BigDecimal EXPENSE_AMOUNT = BigDecimal.valueOf(210.99);
    public static final String EXPENSE_AMOUNT_AS_STRING = EXPENSE_AMOUNT.toString();
    public static final ExpenseType EXPENSE_TYPE = ExpenseType.UNCATEGORIZED;

    private ExpenseTestConstants() {
    }

    public static Expense expense() {
        return new Expense(EXPENSE_ID, TRIP_ID, PERSON_ID, EXPENSE_DESCRIPTION, EXPENSE_DATE, EXPENSE_AMOUNT, EXPENSE_TYPE, newHashSet());
    }

    public static CreateExpenseTO createExpenseTO() {
        return new CreateExpenseTO(TRIP_ID, PERSON_ID, EXPENSE_DESCRIPTION, EXPENSE_DATE_AS_STRING, EXPENSE_AMOUNT_AS_STRING, newArrayList(PERSON_ID));
    }

}
