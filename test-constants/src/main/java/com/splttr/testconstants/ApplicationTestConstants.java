package com.splttr.testconstants;

import java.util.UUID;

import static java.util.UUID.fromString;

public class ApplicationTestConstants {

    public static final String PRINCIPAL_VALUE = "78fc1fc3-2d56-4634-89ef-d208fd54aaaf";
    public static final UUID PRINCIPAL = fromString(PRINCIPAL_VALUE);

    private ApplicationTestConstants() {
    }
}
