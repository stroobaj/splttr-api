package com.splttr.testconstants.trip;

import com.splttr.domain.trip.Trip;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static com.splttr.testconstants.ApplicationTestConstants.PRINCIPAL;
import static com.splttr.testconstants.person.PersonTestConstants.PERSON_ID;
import static java.util.UUID.fromString;

public class TripTestConstants {

    public static final String TRIP_ID_VALUE = "91c54df3-e705-4e84-82e7-1718be826ebb";
    public static final UUID TRIP_ID = fromString(TRIP_ID_VALUE);
    public static final String TRIP_NAME = "Trip name";
    public static final LocalDate LOCAL_DATE = LocalDate.of(2019, 01, 01);
    public static final LocalTime LOCAL_TIME = LocalTime.of(18, 0, 0);
    public static final LocalDateTime CREATION_DATE_TIME = LocalDateTime.of(LOCAL_DATE, LOCAL_TIME);
    public static final LocalDateTime LAST_EDIT_DATE_TIME = LocalDateTime.of(LOCAL_DATE, LOCAL_TIME);

    private TripTestConstants() {
    }

    public static Trip trip() {
        return new Trip(TRIP_ID, PRINCIPAL, TRIP_NAME, CREATION_DATE_TIME, LAST_EDIT_DATE_TIME, newArrayList(PERSON_ID));
    }

}
