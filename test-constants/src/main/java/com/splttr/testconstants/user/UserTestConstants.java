package com.splttr.testconstants.user;

import com.splttr.domain.user.Role;
import com.splttr.domain.user.User;

import java.util.List;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;

public class UserTestConstants {

    public static final UUID USER_ID = UUID.fromString("49e283b4-4088-43e6-92ea-a340a5ee8188");
    private static final String USERNAME = "stroobaj";
    private static final String PASSWORD = "stroobaj";
    private static final String EMAIL = "stroobaj";
    public static final List<Role> ROLES = newArrayList(Role.ROLE_ADMIN);

    private UserTestConstants() {
    }

    public static User user() {
        return new User(USER_ID, USERNAME, PASSWORD, EMAIL, ROLES);
    }

}
