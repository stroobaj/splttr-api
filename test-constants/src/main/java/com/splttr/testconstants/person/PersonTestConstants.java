package com.splttr.testconstants.person;

import com.splttr.applicationapi.person.PersonInfoTO;
import com.splttr.applicationapi.person.PersonTO;
import com.splttr.domain.person.FullName;
import com.splttr.domain.person.Person;

import java.util.UUID;

import static com.splttr.domain.person.Person.newPersonBuilder;
import static com.splttr.testconstants.ApplicationTestConstants.PRINCIPAL;
import static java.util.UUID.fromString;

public class PersonTestConstants {

    public static final String PERSON_ID_VALUE = "77e859c1-d1f1-4419-a621-21b94d4e5f27";
    public static final UUID PERSON_ID = fromString(PERSON_ID_VALUE);
    public static final String PERSON_FIRST_NAME = "Jeroen";
    public static final String PERSON_LAST_NAME = "Stroobants";
    public static final String PERSON_EMAIL = "jeroenstroobants@gmail.com";
    public static final FullName PERSON_FULL_NAME = new FullName(PERSON_FIRST_NAME, PERSON_LAST_NAME);


    private PersonTestConstants() {
    }

    public static Person person() {
        return newPersonBuilder()
                .withPersonId(PERSON_ID)
                .withPrincipal(PRINCIPAL)
                .withFirstName(PERSON_FIRST_NAME)
                .withLastName(PERSON_LAST_NAME)
                .withEmail(PERSON_EMAIL)
                .build();
    }

    public static PersonTO personTO() {
        return new PersonTO(PERSON_ID, PERSON_FULL_NAME, PERSON_EMAIL);
    }

    public static PersonInfoTO personInfoTO() {
        return new PersonInfoTO(PERSON_FIRST_NAME, PERSON_LAST_NAME, PERSON_EMAIL);
    }

}
