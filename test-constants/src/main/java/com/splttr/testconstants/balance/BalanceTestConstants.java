package com.splttr.testconstants.balance;

import com.splttr.applicationapi.balance.PersonBalanceTO;
import com.splttr.applicationapi.balance.UserBalanceTO;

import java.time.LocalDateTime;

import static com.splttr.testconstants.person.PersonTestConstants.personTO;
import static com.splttr.testconstants.trip.TripTestConstants.TRIP_ID;
import static com.splttr.testconstants.trip.TripTestConstants.TRIP_NAME;

public class BalanceTestConstants {

    public static final String BALANCE_VALUE = "50.99";
    public static final String BALANCE_ADVANCE = "50.99";
    public static final String BALANCE_BORROWED = "0";
    public static final LocalDateTime BALANCE_LAST_EDIT_DATE_TIME = LocalDateTime.of(2019, 12, 31, 9, 0);

    private BalanceTestConstants() {
    }

    public static PersonBalanceTO personBalanceTO() {
        return new PersonBalanceTO(personTO(), BALANCE_VALUE, BALANCE_ADVANCE, BALANCE_BORROWED);
    }

    public static UserBalanceTO userBalanceTO() {
        return new UserBalanceTO(TRIP_ID, TRIP_NAME, BALANCE_VALUE, BALANCE_LAST_EDIT_DATE_TIME);
    }

}
