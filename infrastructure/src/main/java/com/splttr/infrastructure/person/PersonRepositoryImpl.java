package com.splttr.infrastructure.person;

import com.splttr.domain.person.Person;
import com.splttr.domain.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class PersonRepositoryImpl implements PersonRepository {

    @Autowired
    private PersonCrudRepository personCrudRepository;

    @Override
    public void save(Person person) {
        personCrudRepository.save(person);
    }

    @Override
    public List<Person> findAllUserPersons(UUID principal) {
        return personCrudRepository.findAllByPrincipal(principal);
    }

    @Override
    public Optional<Person> findByPersonId(UUID personId) {
        return personCrudRepository.findByPersonId(personId);
    }
}
