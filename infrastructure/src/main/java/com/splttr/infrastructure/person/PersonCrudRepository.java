package com.splttr.infrastructure.person;

import com.splttr.domain.person.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonCrudRepository extends CrudRepository<Person, UUID> {

    @Override
    List<Person> findAll();

    List<Person> findAllByPrincipal(UUID principal);

    Optional<Person> findByPersonId(UUID personId);

}
