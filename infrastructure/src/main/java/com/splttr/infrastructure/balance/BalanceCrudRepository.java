package com.splttr.infrastructure.balance;

import com.splttr.domain.balance.Balance;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface BalanceCrudRepository extends CrudRepository<Balance, UUID> {

    List<Balance> findByTripId(UUID tripId);

    List<Balance> findByPersonId(UUID personId);

    Balance findByTripIdAndPersonId(UUID tripId, UUID personId);

}
