package com.splttr.infrastructure.balance;

import com.splttr.domain.balance.Balance;
import com.splttr.domain.balance.BalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class BalanceRepositoryImpl implements BalanceRepository {

    @Autowired
    private BalanceCrudRepository balanceCrudRepository;

    @Override
    public void save(Balance balance) {
        balanceCrudRepository.save(balance);
    }

    @Override
    public List<Balance> findByTripId(UUID tripId) {
        return balanceCrudRepository.findByTripId(tripId);
    }

    @Override
    public List<Balance> findByPersonId(UUID personId) {
        return balanceCrudRepository.findByPersonId(personId);
    }

    @Override
    public Balance findByBalanceByTripIdAndPersonId(UUID tripId, UUID personId) {
        return balanceCrudRepository.findByTripIdAndPersonId(tripId, personId);
    }

}
