package com.splttr.infrastructure;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"com.splttr"})
@ComponentScan(basePackages = {"com.splttr.infrastructure", "com.splttr.commons.security"})
@EntityScan(basePackages = {"com.splttr"})
@EnableAutoConfiguration
public class SplttrInfrastructureConfiguration {

}
