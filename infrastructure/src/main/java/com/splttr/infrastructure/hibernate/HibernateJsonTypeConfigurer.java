package com.splttr.infrastructure.hibernate;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface HibernateJsonTypeConfigurer {

    ObjectMapper getJsonTypeConfigurerObjectMapper();

}
