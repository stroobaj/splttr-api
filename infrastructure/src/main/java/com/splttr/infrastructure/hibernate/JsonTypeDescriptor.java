package com.splttr.infrastructure.hibernate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.hibernate.engine.jdbc.CharacterStream;
import org.hibernate.engine.jdbc.internal.CharacterStreamImpl;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;
import org.hibernate.type.descriptor.java.MutableMutabilityPlan;
import org.hibernate.usertype.DynamicParameterizedType;

import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Properties;

public class JsonTypeDescriptor extends AbstractTypeDescriptor<Object> implements DynamicParameterizedType {

    private static ObjectMapper objectMapper;
    private Class<?> jsonObjectClass;

    JsonTypeDescriptor() {
        super(Object.class, new MutableMutabilityPlan<Object>() {
            @Override
            protected Object deepCopyNotNull(Object value) {
                try {
                    return JsonTypeDescriptor.clone(value);
                } catch (IOException e) {
                    throw new IllegalArgumentException("The given json object value " + value +
                        " could not be cloned", e);
                }
            }
        });
    }

    @Override
    public void setParameterValues(Properties parameters) {
        jsonObjectClass = ((ParameterType) parameters.get(PARAMETER_TYPE))
            .getReturnedClass();

    }

    @Override
    public boolean areEqual(Object one, Object another) {
        if (one == another) {
            return true;
        }
        if (one == null || another == null) {
            return false;
        }
        return toJsonNode(toString(one)).equals(toJsonNode(toString(another)));
    }

    @Override
    public String toString(Object value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("The given Json object value: "
                + value + " cannot be transformed to a String", e);
        }
    }

    @Override
    public Object fromString(String string) {
        try {
            return objectMapper.readValue(string, jsonObjectClass);
        } catch (IOException e) {
            throw new IllegalArgumentException("The given string value: "
                + string + " cannot be transformed to Json object", e);
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public <X> X unwrap(Object value, Class<X> type, WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (String.class.isAssignableFrom(type)) {
            return (X) toString(value);
        }
        if (CharacterStream.class.isAssignableFrom(type)) {
            return (X) new CharacterStreamImpl(toString(value));
        }
        if (Object.class.isAssignableFrom(type)) {
            return (X) toJsonNode(toString(value));
        }
        throw unknownUnwrap(type);
    }

    @Override
    public <X> Object wrap(X value, WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (Clob.class.isInstance(value)) {
            try {
                return fromString(IOUtils.toString(Clob.class.cast(value).getCharacterStream()));
            } catch (IOException | SQLException e) {
                throw new IllegalArgumentException("Cannot read value from underlying CLOB: " + value, e);
            }
        }
        return fromString(value.toString());
    }

    private JsonNode toJsonNode(String value) {
        try {
            return objectMapper.readTree(value);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Object clone(Object value) throws IOException {
        return objectMapper.readValue(objectMapper.writeValueAsString(value), value.getClass());
    }

    public static void setObjectMapper(ObjectMapper objectMapper) {
        JsonTypeDescriptor.objectMapper = objectMapper;
    }
}
