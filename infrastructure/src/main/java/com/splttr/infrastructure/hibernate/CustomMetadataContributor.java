package com.splttr.infrastructure.hibernate;

import org.hibernate.boot.model.TypeDefinition;
import org.hibernate.boot.spi.InFlightMetadataCollector;
import org.hibernate.boot.spi.MetadataContributor;
import org.jboss.jandex.IndexView;

import static java.util.Collections.EMPTY_MAP;

public class CustomMetadataContributor implements MetadataContributor {

    @Override
    @SuppressWarnings("unchecked")
    public void contribute(InFlightMetadataCollector metadataCollector, IndexView jandexIndex) {
        metadataCollector.addTypeDefinition(new TypeDefinition("json", JsonStringType.class, new String[] {"json"}, EMPTY_MAP));
    }
}
