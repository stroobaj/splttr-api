package com.splttr.infrastructure.hibernate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class HibernateJsonTypeAutoConfiguration {

    @Autowired
    private ApplicationContext applicationContext;

    private HibernateJsonTypeConfigurer hibernateJsonTypeConfigurer;

    @Autowired(required = false)
    public void setConfigurers(HibernateJsonTypeConfigurer hibernateJsonTypeConfigurer) {
        this.hibernateJsonTypeConfigurer = hibernateJsonTypeConfigurer;
    }

    @PostConstruct
    public void initialize() {
        if (hibernateJsonTypeConfigurer != null) {
            JsonTypeDescriptor.setObjectMapper(hibernateJsonTypeConfigurer.getJsonTypeConfigurerObjectMapper());
        } else {
            JsonTypeDescriptor.setObjectMapper(applicationContext.getBean(ObjectMapper.class));
        }
    }
}
