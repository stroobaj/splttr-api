package com.splttr.infrastructure.trip;

import com.splttr.domain.trip.Trip;
import com.splttr.domain.trip.TripId;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TripCrudRepository extends CrudRepository<Trip, TripId> {

    @Override
    List<Trip> findAll();

    List<Trip> findByPrincipal(UUID principal);

    Trip findByTripId(UUID tripId);

    Optional<Trip> findByTripIdAndPrincipal(UUID tripId, UUID principal);

}
