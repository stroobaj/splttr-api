package com.splttr.infrastructure.trip;

import com.splttr.domain.trip.Trip;
import com.splttr.domain.trip.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class TripRepositoryImpl implements TripRepository {

    @Autowired
    private TripCrudRepository tripCrudRepository;

    @Override
    public void save(Trip trip) {
        tripCrudRepository.save(trip);
    }

    @Override
    public Trip findOne(UUID tripId) {
        return tripCrudRepository.findByTripId(tripId);
    }

    @Override
    public List<Trip> findAllUserTrips(UUID principal) {
        return tripCrudRepository.findByPrincipal(principal);
    }

    @Override
    public List<UUID> findAllPersonsOnTrip(UUID tripId) {
        return tripCrudRepository.findByTripId(tripId).getPersons();
    }

    @Override
    public Optional<Trip> findUserTripByTripId(UUID tripId, UUID principal) {
        return tripCrudRepository.findByTripIdAndPrincipal(tripId, principal);
    }

}
