package com.splttr.infrastructure.expense;

import com.splttr.domain.expense.Expense;
import com.splttr.domain.expense.ExpenseAllocation;
import com.splttr.domain.expense.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class ExpenseRepositoryImpl implements ExpenseRepository {

    @Autowired
    private ExpenseCrudRepository expenseCrudRepository;

    @Autowired
    private ExpenseDistributionCrudRepository expenseDistributionCrudRepository;

    @Override
    public void save(Expense expense) {
        expenseCrudRepository.save(expense);
    }

    @Override
    public void save(List<ExpenseAllocation> expenseAllocations) {
        expenseDistributionCrudRepository.saveAll(expenseAllocations);
    }

    @Override
    public List<Expense> findByTripId(UUID tripId) {
        return expenseCrudRepository.findByTripId(tripId);
    }

}
