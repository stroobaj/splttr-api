package com.splttr.infrastructure.expense;

import com.splttr.domain.expense.ExpenseAllocation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ExpenseDistributionCrudRepository extends CrudRepository<ExpenseAllocation, UUID> {
}
