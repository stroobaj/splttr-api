package com.splttr.infrastructure.expense;

import com.splttr.domain.expense.Expense;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface ExpenseCrudRepository extends CrudRepository<Expense, UUID> {

    List<Expense> findByTripId(UUID tripId);

}
