package com.splttr.infrastructure.user;

import com.splttr.domain.user.User;
import com.splttr.domain.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserRepositoryImpl implements UserRepository {

    private final UserCrudRepository userCrudRepository;

    @Autowired
    public UserRepositoryImpl(UserCrudRepository userCrudRepository) {
        this.userCrudRepository = userCrudRepository;
    }

    @Override
    public void save(User user) {
        userCrudRepository.save(user);
    }

    @Override
    public boolean existsByUsername(String username) {
        return userCrudRepository.existsByUsername(username);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userCrudRepository.findByUsername(username);
    }

    @Override
    public void deleteByUsername(String username) {
        userCrudRepository.deleteByUsername(username);
    }
}
