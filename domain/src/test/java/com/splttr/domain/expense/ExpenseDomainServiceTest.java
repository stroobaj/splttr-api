package com.splttr.domain.expense;

import com.splttr.domain.trip.TripRepository;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoRule;

import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.UUID.randomUUID;
import static org.mockito.Mockito.when;
import static org.mockito.junit.MockitoJUnit.rule;

public class ExpenseDomainServiceTest {

    @Rule
    public MockitoRule mockitoRule = rule();

    @Mock
    private TripRepository tripRepository;

    @InjectMocks
    private ExpenseDomainService domainService;

    @Test
    public void distributeExpense() {
        UUID tripId = randomUUID();
        UUID payerId = randomUUID();
        when(tripRepository.findAllPersonsOnTrip(tripId)).thenReturn(newArrayList(randomUUID(), randomUUID(), payerId, randomUUID(), randomUUID()));

//        domainService.allocateExpense(
//                randomUUID(),
//                tripId,
//                payerId,
//                BigDecimal.valueOf(755.63),
//                newArrayList(randomUUID(), randomUUID(), randomUUID(), randomUUID(), randomUUID())
//        );
    }
}
