package com.splttr.domain.balance;


import java.util.List;
import java.util.UUID;

public interface BalanceRepository {

    void save(Balance balance);

    List<Balance> findByTripId(UUID tripId);

    List<Balance> findByPersonId(UUID personId);

    Balance findByBalanceByTripIdAndPersonId(UUID tripId, UUID personid);

}
