package com.splttr.domain.balance;

import com.splttr.commons.equals.EqualByStateObject;

import java.math.BigDecimal;
import java.util.List;

public class ExpenseDistributions extends EqualByStateObject {

    private BigDecimal advance;
    private List<BigDecimal> refunds;

    private ExpenseDistributions() {
    }

    public ExpenseDistributions(BigDecimal advance, List<BigDecimal> refunds) {
        this.advance = advance;
        this.refunds = refunds;
    }

    public BigDecimal getAdvance() {
        return advance;
    }

    public List<BigDecimal> getRefunds() {
        return refunds;
    }

}
