package com.splttr.domain.balance;

import com.splttr.domain.expense.ExpenseAllocation;
import com.splttr.domain.trip.Trip;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

import static java.math.BigDecimal.ZERO;
import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;

@Component
public class BalanceDomainService {

    private BalanceRepository balanceRepository;

    public BalanceDomainService(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public void createInitialBalances(Trip trip) {
        trip.getPersons().forEach(personId -> balanceRepository.save(createBalance(trip.getTripId(), personId)));
    }

    private Balance createBalance(UUID tripId, UUID personId) {
        return new Balance(randomUUID(), tripId, personId, ZERO, ZERO, ZERO, now());
    }

    public void updateTripBalances(UUID tripId, Set<ExpenseAllocation> expenseAllocations) {
        expenseAllocations.forEach(expenseAllocation -> {
            Balance balance = balanceRepository.findByBalanceByTripIdAndPersonId(tripId, expenseAllocation.getPersonId());
            BigDecimal amount = expenseAllocation.getAmount();

            if (isPayerAllocation(amount)) {
                balance.updateBalance(amount, amount, ZERO);
            } else if (!isPayerAllocation(amount)) {
                balance.updateBalance(amount, ZERO, amount);
            }
        });
    }

    private boolean isPayerAllocation(BigDecimal amount) {
        return amount.compareTo(ZERO) > 0;
    }

}
