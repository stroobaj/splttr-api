package com.splttr.domain.balance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Entity
@Table(name = "balance")
public class Balance {

    @Id
    @Column(name = "balance_id")
    private UUID balanceId;

    @Column(name = "trip_id")
    private UUID tripId;

    @Column(name = "person_id")
    private UUID personId;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "advance")
    private BigDecimal advance;

    @Column(name = "borrowed")
    private BigDecimal borrowed;

    @Column(name = "last_edit_date_time")
    private LocalDateTime lastEditDateTime;

    private Balance() {
    }

    public Balance(UUID balanceId,
                   UUID tripId,
                   UUID personId,
                   BigDecimal balance,
                   BigDecimal advance,
                   BigDecimal borrowed,
                   LocalDateTime lastEditDateTime) {
        this.balanceId = balanceId;
        this.tripId = tripId;
        this.personId = personId;
        this.balance = balance;
        this.advance = advance;
        this.borrowed = borrowed;
        this.lastEditDateTime = lastEditDateTime;
    }

    public UUID getBalanceId() {
        return balanceId;
    }

    public UUID getTripId() {
        return tripId;
    }

    public UUID getPersonId() {
        return personId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public BigDecimal getAdvance() {
        return advance;
    }

    public BigDecimal getBorrowed() {
        return borrowed;
    }

    public LocalDateTime getLastEditDateTime() {
        return lastEditDateTime;
    }

    public void updateBalance(BigDecimal expenseShare, BigDecimal advancedAmount, BigDecimal borrowedAmount) {
        balance = balance.add(expenseShare);
        advance = advance.add(advancedAmount);
        borrowed = borrowed.add(borrowedAmount.negate());
        lastEditDateTime = now();
    }

}
