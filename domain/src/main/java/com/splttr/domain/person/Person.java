package com.splttr.domain.person;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @Column(name = "person_id")
    private UUID personId;

    @Column(name = "principal")
    private UUID principal;

    @Embedded
    private FullName fullName;

    @Column(name = "email")
    private String email;

    private Person() {
    }

    private Person(Builder builder) {
        personId = builder.personId;
        principal = builder.principal;
        fullName = createFullName(builder.firstName, builder.lastName);
        email = builder.email;
    }

    public static Builder newPersonBuilder() {
        return new Builder();
    }

    public UUID getPersonId() {
        return personId;
    }

    public UUID getPrincipal() {
        return principal;
    }

    public FullName getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    private FullName createFullName(String firstName, String lastName) {
        return new FullName(firstName, lastName);
    }


    public static final class Builder {
        private UUID personId;
        private UUID principal;
        private String firstName;
        private String lastName;
        private String email;

        private Builder() {
        }

        public Builder withPersonId(UUID personId) {
            this.personId = personId;
            return this;
        }

        public Builder withPrincipal(UUID principal) {
            this.principal = principal;
            return this;
        }

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }
}
