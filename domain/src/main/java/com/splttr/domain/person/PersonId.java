package com.splttr.domain.person;

import com.splttr.domain.EntityId;

import javax.persistence.Embeddable;
import java.util.UUID;

@Embeddable
public class PersonId extends EntityId {

    private PersonId() {
    }

    public PersonId(UUID principal, UUID id) {
        super(principal, id);
    }
}
