package com.splttr.domain.person;

import com.splttr.commons.equals.EqualByStateObject;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FullName extends EqualByStateObject {

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;


    private FullName() {
    }

    public FullName(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
