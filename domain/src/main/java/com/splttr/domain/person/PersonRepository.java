package com.splttr.domain.person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonRepository {

    void save(Person person);

    List<Person> findAllUserPersons(UUID principal);

    Optional<Person> findByPersonId(UUID personId);

}
