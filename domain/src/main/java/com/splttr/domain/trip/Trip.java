package com.splttr.domain.trip;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static java.time.LocalDateTime.now;
import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name = "trip")
public class Trip {

    @Id
    @Column(name = "trip_id")
    private UUID tripId;

    @Column(name = "principal")
    private UUID principal;

    @Column(name = "name")
    private String name;

    @Column(name = "creation_date_time")
    private LocalDateTime creationDateTime;

    @Column(name = "last_edit_date_time")
    private LocalDateTime lastEditDateTime;

    @ElementCollection(fetch = EAGER)
    @CollectionTable(name = "person_on_trip", joinColumns = @JoinColumn(name = "trip_id", referencedColumnName = "trip_id"))
    @Column(name = "person")
    private List<UUID> persons;

    private Trip() {
    }

    public Trip(UUID tripId, UUID principal,
                String name,
                LocalDateTime creationDateTime,
                LocalDateTime lastEditDateTime,
                List<UUID> persons) {
        this.tripId = tripId;
        this.principal = principal;
        this.name = name;
        this.creationDateTime = creationDateTime;
        this.lastEditDateTime = lastEditDateTime;
        this.persons = persons;
    }

    public void updateLastEdit() {
        lastEditDateTime = now();
    }

    public UUID getTripId() {
        return tripId;
    }

    public UUID getPrincipal() {
        return principal;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public LocalDateTime getLastEditDateTime() {
        return lastEditDateTime;
    }

    public List<UUID> getPersons() {
        return persons;
    }

}
