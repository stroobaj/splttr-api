package com.splttr.domain.trip;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TripRepository {

    void save(Trip trip);

    Trip findOne(UUID tripId);

    List<Trip> findAllUserTrips(UUID principal);

    List<UUID> findAllPersonsOnTrip(UUID tripId);

    Optional<Trip> findUserTripByTripId(UUID tripId, UUID principal);

}
