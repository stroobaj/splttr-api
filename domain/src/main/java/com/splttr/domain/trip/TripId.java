package com.splttr.domain.trip;

import com.splttr.domain.EntityId;

import javax.persistence.Embeddable;
import java.util.UUID;

@Embeddable
public class TripId extends EntityId {

    private TripId() {
    }

    public TripId(UUID principal, UUID id) {
        super(principal, id);
    }
}
