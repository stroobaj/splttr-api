package com.splttr.domain.trip;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;

@Component
public class TripDomainService {

    private TripRepository tripRepository;

    public TripDomainService(TripRepository tripRepository) {
        this.tripRepository = tripRepository;
    }

    public void updateLastEditDateTime(UUID tripId) {
        tripRepository.findOne(tripId).updateLastEdit();
    }

    public void saveTrip(Trip trip) {
        tripRepository.save(trip);
    }

    public List<Trip> findAllUserTrips(UUID principal) {
        return tripRepository.findAllUserTrips(principal);
    }

    public Optional<Trip> findUserTripByTripId(UUID tripId, UUID principal) {
        return tripRepository.findUserTripByTripId(tripId, principal);
    }

    public String getTripName(UUID tripId, UUID principal) {
        return tripRepository.findUserTripByTripId(tripId, principal)
                .orElseThrow(() -> new IllegalStateException(format("Trip with id %s does not exist.", tripId)))
                .getName();
    }
}
