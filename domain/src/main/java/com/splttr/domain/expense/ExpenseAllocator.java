package com.splttr.domain.expense;

import com.splttr.domain.balance.ExpenseDistributions;
import com.splttr.domain.expense.strategy.DistributeExpenseStrategy;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.google.common.collect.Sets.newHashSet;
import static java.math.BigDecimal.valueOf;
import static java.util.UUID.randomUUID;

@Component
public class ExpenseAllocator {

    private List<DistributeExpenseStrategy> distributeExpenseStrategies;

    public ExpenseAllocator(List<DistributeExpenseStrategy> distributeExpenseStrategies) {
        this.distributeExpenseStrategies = distributeExpenseStrategies;
    }

    public Set<ExpenseAllocation> allocateExpense(UUID expenseId, UUID payerId, BigDecimal amountToDistribute, List<UUID> partakers) {
        Set<ExpenseAllocation> expenseAllocations = newHashSet();

        ExpenseDistributions expenseDistributions = distributeExpenseStrategies.stream()
                .filter(distributeExpenseStrategy -> distributeExpenseStrategy.canHandle(payerId, partakers))
                .map(distributeExpenseStrategy -> distributeExpenseStrategy.handle(payerId, amountToDistribute, valueOf(partakers.size())))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No suitable DistributeExpenseStrategy found"));

        Iterator<BigDecimal> refunds = expenseDistributions.getRefunds().iterator();

        partakers.forEach(partaker -> {
            if (!isPayer(payerId, partaker)) {
                expenseAllocations.add(new ExpenseAllocation(randomUUID(), expenseId, partaker, refunds.next().negate()));
            }
        });

        expenseAllocations.add(new ExpenseAllocation(randomUUID(), expenseId, payerId, expenseDistributions.getAdvance()));

        return expenseAllocations;
    }

    private boolean isPayer(UUID payerId, UUID personId) {
        return payerId.equals(personId);
    }

}
