package com.splttr.domain.expense;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "expense_allocation")
public class ExpenseAllocation {

    @Id
    @Column(name = "expense_allocation_id")
    private UUID expenseAllocationId;

    @Column(name = "expense_id")
    private UUID expenseId;

    @Column(name = "person_id")
    private UUID personId;

    @Column(name = "amount")
    private BigDecimal amount;

    private ExpenseAllocation() {
    }

    public ExpenseAllocation(UUID expenseAllocationId, UUID expenseId, UUID personId, BigDecimal amount) {
        this.expenseAllocationId = expenseAllocationId;
        this.expenseId = expenseId;
        this.personId = personId;
        this.amount = amount;
    }

    public UUID getExpenseAllocationId() {
        return expenseAllocationId;
    }

    public UUID getExpenseId() {
        return expenseId;
    }

    public UUID getPersonId() {
        return personId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
