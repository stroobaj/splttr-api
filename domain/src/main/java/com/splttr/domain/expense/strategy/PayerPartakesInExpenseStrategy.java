package com.splttr.domain.expense.strategy;

import com.splttr.domain.balance.ExpenseDistributions;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.splttr.domain.expense.RemainingAmountAllocator.allocateRemainingAmount;
import static java.util.Arrays.asList;
import static java.util.Collections.fill;

@Component
public class PayerPartakesInExpenseStrategy implements DistributeExpenseStrategy {

    @Override
    public boolean canHandle(UUID payerId, List<UUID> partakers) {
        return partakers.contains(payerId);
    }

    @Override
    public ExpenseDistributions handle(UUID payerId, BigDecimal amountToDistribute, BigDecimal numberOfPersonsOnExpense) {
        BigDecimal roundedRefund = amountToDistribute.divide(numberOfPersonsOnExpense, 2, BigDecimal.ROUND_DOWN);
        BigDecimal remainingAmountToAllocate = amountToDistribute.remainder(roundedRefund);
        BigDecimal advancePayment = amountToDistribute.subtract(roundedRefund);

        List<BigDecimal> refunds = asList(new BigDecimal[numberOfPersonsOnExpense.intValue() - 1]);
        fill(refunds, roundedRefund);

        allocateRemainingAmount(numberOfPersonsOnExpense.intValue() - 1, remainingAmountToAllocate, refunds);

        return new ExpenseDistributions(advancePayment, refunds);
    }

}
