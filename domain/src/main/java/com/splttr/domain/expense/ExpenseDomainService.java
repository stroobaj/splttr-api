package com.splttr.domain.expense;

import com.splttr.domain.balance.BalanceDomainService;
import com.splttr.domain.trip.TripDomainService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static java.lang.Double.parseDouble;
import static java.math.BigDecimal.valueOf;
import static java.time.LocalDate.parse;

@Component
public class ExpenseDomainService {

    private BalanceDomainService balanceDomainService;
    private TripDomainService tripDomainService;
    private ExpenseRepository expenseRepository;
    private ExpenseAllocator expenseAllocator;

    public ExpenseDomainService(BalanceDomainService balanceDomainService,
                                TripDomainService tripDomainService,
                                ExpenseRepository expenseRepository,
                                ExpenseAllocator expenseAllocator) {
        this.balanceDomainService = balanceDomainService;
        this.tripDomainService = tripDomainService;
        this.expenseRepository = expenseRepository;
        this.expenseAllocator = expenseAllocator;
    }

    public void createExpense(UUID tripId, UUID payerId, String amountToDistribute, String date, String description, List<UUID> partakers) {
        UUID expenseId = UUID.randomUUID();

        Set<ExpenseAllocation> expenseAllocations = expenseAllocator.allocateExpense(expenseId, payerId, new BigDecimal(amountToDistribute), partakers);

        expenseRepository.save(new Expense(
                expenseId,
                tripId,
                payerId,
                description,
                parse(date),
                valueOf(parseDouble(amountToDistribute)),
                ExpenseType.UNCATEGORIZED,
                expenseAllocations));

        balanceDomainService.updateTripBalances(tripId, expenseAllocations);
        tripDomainService.updateLastEditDateTime(tripId);
    }

//    private Set<ExpenseAllocation> allocateExpense(UUID expenseId, UUID tripId, UUID payerId, BigDecimal amountToDistribute, List<UUID> partakers) {
//        Set<ExpenseAllocation> expenseAllocations = newHashSet();
//
//        ExpenseDistributions expenseDistributions = distributeExpenseStrategies.stream()
//                .filter(distributeExpenseStrategy -> distributeExpenseStrategy.canHandle(payerId, partakers))
//                .map(distributeExpenseStrategy -> distributeExpenseStrategy.handle(payerId, amountToDistribute, valueOf(partakers.size())))
//                .findFirst()
//                .orElseThrow(() -> new IllegalArgumentException("No suitable DistributeExpenseStrategy found"));
//
//        Iterator<BigDecimal> refunds = expenseDistributions.getRefunds().iterator();
//
//        partakers.forEach(partaker -> {
//            if (!isPayer(payerId, partaker)) {
//                expenseAllocations.add(new ExpenseAllocation(randomUUID(), expenseId, partaker, refunds.next().negate()));
//            }
//        });
//
//        expenseAllocations.add(new ExpenseAllocation(randomUUID(), expenseId, payerId, expenseDistributions.getAdvance()));
//
//        balanceDomainService.updateTripBalances(tripId, expenseAllocations);
//        return expenseAllocations;
//    }
//
//    private boolean isPayer(UUID payerId, UUID personId) {
//        return payerId.equals(personId);
//    }

}
