package com.splttr.domain.expense;

import java.math.BigDecimal;
import java.util.List;

import static java.math.BigDecimal.valueOf;

public class RemainingAmountAllocator {

    public static void allocateRemainingAmount(int numberOfPersonsToAllocate, BigDecimal amountToAllocate, List<BigDecimal> refunds) {
        int person = numberOfPersonsToAllocate - 1;

        while (amountToAllocate.compareTo(BigDecimal.ZERO) > 0) {
            refunds.set(person, refunds.get(person).add(valueOf(0.01)));
            amountToAllocate = amountToAllocate.subtract(valueOf(0.01));
            person--;
        }
    }

}
