package com.splttr.domain.expense.strategy;

import com.splttr.domain.balance.ExpenseDistributions;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface DistributeExpenseStrategy {

    boolean canHandle(UUID payerId, List<UUID> partakers);

    ExpenseDistributions handle(UUID payerId, BigDecimal amountToDistribute, BigDecimal numberOfPersonsOnExpense);

}
