package com.splttr.domain.expense;

import java.util.List;
import java.util.UUID;

public interface ExpenseRepository {

    void save(Expense expense);

    void save(List<ExpenseAllocation> expenseAllocations);

    List<Expense> findByTripId(UUID tripId);

}
