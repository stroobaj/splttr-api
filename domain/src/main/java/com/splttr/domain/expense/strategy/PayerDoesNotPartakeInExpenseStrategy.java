package com.splttr.domain.expense.strategy;

import com.splttr.domain.balance.ExpenseDistributions;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static com.splttr.domain.expense.RemainingAmountAllocator.allocateRemainingAmount;
import static java.util.Arrays.asList;
import static java.util.Collections.fill;

@Component
public class PayerDoesNotPartakeInExpenseStrategy implements DistributeExpenseStrategy {

    @Override
    public boolean canHandle(UUID payerId, List<UUID> partakers) {
        return !partakers.contains(payerId);
    }

    @Override
    public ExpenseDistributions handle(UUID payerId, BigDecimal amountToDistribute, BigDecimal numberOfPersonsOnExpense) {
        BigDecimal roundedRefund = amountToDistribute.divide(numberOfPersonsOnExpense, 2, BigDecimal.ROUND_DOWN);
        BigDecimal remainingAmountToAllocate = amountToDistribute.remainder(roundedRefund);

        List<BigDecimal> refunds = asList(new BigDecimal[numberOfPersonsOnExpense.intValue()]);
        fill(refunds, roundedRefund);

        allocateRemainingAmount(numberOfPersonsOnExpense.intValue(), remainingAmountToAllocate, refunds);

        return new ExpenseDistributions(amountToDistribute, refunds);
    }


}
