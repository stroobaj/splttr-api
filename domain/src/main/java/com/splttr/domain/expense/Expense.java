package com.splttr.domain.expense;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

import static com.google.common.collect.Sets.newHashSet;

@Entity
@Table(name = "expense")
public class Expense {

    @Id
    @Column(name = "expense_id")
    private UUID expenseId;

    @Column(name = "trip_id")
    private UUID tripId;

    @Column(name = "payer_id")
    private UUID payerId;

    @Column(name = "description")
    private String description;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "amount")
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private ExpenseType category;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "expense_id")
    private Set<ExpenseAllocation> expenseAllocations = newHashSet();

    private Expense() {
    }

    public Expense(UUID expenseId,
                   UUID tripId,
                   UUID payerId,
                   String description,
                   LocalDate date,
                   BigDecimal amount,
                   ExpenseType category,
                   Set<ExpenseAllocation> expenseAllocations) {
        this.expenseId = expenseId;
        this.tripId = tripId;
        this.payerId = payerId;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.category = category;
        this.expenseAllocations = expenseAllocations;
    }

    public UUID getExpenseId() {
        return expenseId;
    }

    public UUID getTripId() {
        return tripId;
    }

    public UUID getPayerId() {
        return payerId;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ExpenseType getCategory() {
        return category;
    }

    public Set<ExpenseAllocation> getExpenseAllocations() {
        return expenseAllocations;
    }
}
