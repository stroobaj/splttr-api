package com.splttr.domain.expense;

public enum ExpenseType {
    ENTERTAINMENT,
    DINING_OUT,
    GROCERIES,
    LIQUOR,
    TRANSPORTATION,
    UTILITIES,
    UNCATEGORIZED
}
