package com.splttr.domain.user;

import java.util.Optional;

public interface UserRepository {

    void save(User user);

    boolean existsByUsername(String username);

    Optional<User> findByUsername(String username);

    void deleteByUsername(String username);

}
