package com.splttr.security;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.splttr"})
//@EnableJpaRepositories(basePackages = {"com.splttr"})
@EnableAutoConfiguration
public class SecurityConfiguration {

}
