package com.splttr.applicationservice.user;

import com.splttr.applicationapi.user.UserDataTO;
import com.splttr.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static com.splttr.domain.user.Role.ROLE_USER;

@Component
public class UserMapper {

    @Autowired
    private PasswordEncoder passwordEncoder;

    private UserMapper() {
    }

    public User mapToUser(UserDataTO userDataTO) {
        UUID userId = UUID.randomUUID();
        return new User(userId,
                userDataTO.getUsername(),
                passwordEncoder.encode(userDataTO.getPassword()),
                userDataTO.getEmail(),
                newArrayList(ROLE_USER));
    }
}
