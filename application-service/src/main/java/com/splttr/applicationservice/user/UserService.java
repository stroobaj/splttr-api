package com.splttr.applicationservice.user;

import com.splttr.applicationapi.response.ResponseMessage;
import com.splttr.applicationapi.user.LoginCredentials;
import com.splttr.applicationapi.user.UserDataTO;
import com.splttr.domain.person.Person;
import com.splttr.domain.person.PersonRepository;
import com.splttr.domain.user.User;
import com.splttr.domain.user.UserRepository;
import com.splttr.security.jwt.JwtProvider;
import com.splttr.security.jwt.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.UUID;

import static com.splttr.domain.person.Person.newPersonBuilder;

@Component
@Transactional
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    public ResponseEntity<?> login(LoginCredentials loginCredentials) {
        String username = loginCredentials.getUsername();
        String password = loginCredentials.getPassword();

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }

    public ResponseEntity<?> signup(UserDataTO userDataTO) {
        User user = userMapper.mapToUser(userDataTO);

        if (!userRepository.existsByUsername(user.getUsername())) {
            UUID userId = user.getUserId();
            Person person = newPersonBuilder()
                    .withPersonId(userId)
                    .withPrincipal(userId)
                    .withFirstName(userDataTO.getFirstName())
                    .withLastName(userDataTO.getLastName())
                    .withEmail(userDataTO.getEmail())
                    .build();
            userRepository.save(user);
            personRepository.save(person);
            return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ResponseMessage("Username is already in use"), HttpStatus.BAD_REQUEST);
        }
    }

    public void delete(String username) {
        userRepository.deleteByUsername(username);
    }

    public User search(String username) {
        return userRepository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User '" + username + "' not found"));
//                .orElseThrow(() -> new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND));
    }

    public User whoami(HttpServletRequest req) {
        String username = jwtProvider.getUsername(jwtProvider.getJwt(req));
        return userRepository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User '" + username + "' not found"));
    }

}
