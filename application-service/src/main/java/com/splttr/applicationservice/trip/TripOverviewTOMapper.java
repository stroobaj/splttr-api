package com.splttr.applicationservice.trip;

import com.splttr.applicationapi.balance.PersonBalanceTO;
import com.splttr.applicationapi.expense.ExpenseInfoTO;
import com.splttr.applicationapi.person.PersonTO;
import com.splttr.applicationapi.trip.TripOverviewTO;
import com.splttr.applicationservice.balance.PersonBalanceTOMapper;
import com.splttr.applicationservice.expense.ExpenseInfoTOMapper;
import com.splttr.applicationservice.person.PersonTOMapper;
import com.splttr.domain.trip.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class TripOverviewTOMapper {

    @Autowired
    private PersonTOMapper personTOMapper;

    @Autowired
    private ExpenseInfoTOMapper expenseInfoTOMapper;

    @Autowired
    private PersonBalanceTOMapper personBalanceTOMapper;

    private TripOverviewTOMapper() {
    }

    public TripOverviewTO mapToTripOverviewTO(Trip trip) {
        UUID tripId = trip.getTripId();
        List<PersonTO> personTOS = personTOMapper.mapToPersonTOs(trip.getPersons());
        List<PersonBalanceTO> personBalanceTOs = personBalanceTOMapper.mapToPersonBalanceTOs(tripId);
        List<ExpenseInfoTO> expenseInfoTOs = expenseInfoTOMapper.mapToExpenseInfoTOs(tripId);

        return new TripOverviewTO(
                tripId,
                trip.getName(),
                trip.getCreationDateTime(),
                trip.getLastEditDateTime(),
                personTOS,
                personBalanceTOs,
                expenseInfoTOs);
    }

}
