package com.splttr.applicationservice.trip;

import com.splttr.applicationapi.trip.CreateTripTO;
import com.splttr.applicationapi.trip.TripOverviewTO;
import com.splttr.domain.balance.BalanceDomainService;
import com.splttr.domain.person.PersonRepository;
import com.splttr.domain.trip.Trip;
import com.splttr.domain.trip.TripDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static java.time.LocalDateTime.now;
import static java.util.Comparator.comparing;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toList;

@Component
@Transactional
public class TripApplicationService {

    @Autowired
    private TripDomainService tripDomainService;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private BalanceDomainService balanceDomainService;

    @Autowired
    private TripOverviewTOMapper tripOverviewTOMapper;

    public void createUserTrip(CreateTripTO createTripTO, UUID principal) {
        List<UUID> persons = createTripTO.getPersons().stream()
                .map(personId -> personRepository
                        .findByPersonId(personId)
                        .orElseThrow(() -> new IllegalStateException("Person should be present for id: " + personId))
                        .getPersonId())
                .collect(toList());

        Trip trip = new Trip(randomUUID(), principal, createTripTO.getName(), now(), now(), persons);

        tripDomainService.saveTrip(trip);
        balanceDomainService.createInitialBalances(trip);
    }

    public List<TripOverviewTO> getAllUserTrips(UUID principal) {
        return tripDomainService.findAllUserTrips(principal)
                .stream()
                .map((tripOverviewTOMapper::mapToTripOverviewTO))
                .sorted(comparing(TripOverviewTO::getLastEditDateTime).reversed())
                .collect(toList());
    }

    public TripOverviewTO getUserTripById(UUID tripId, UUID principal) {
        Trip trip = tripDomainService.findUserTripByTripId(tripId, principal)
                .orElseThrow(() -> new IllegalStateException(format("Trip with id %s does not exist.", tripId)));
        return tripOverviewTOMapper.mapToTripOverviewTO(trip);
    }

    public String getTripName(UUID tripId, UUID principal) {
        return tripDomainService.getTripName(tripId, principal);
    }

}
