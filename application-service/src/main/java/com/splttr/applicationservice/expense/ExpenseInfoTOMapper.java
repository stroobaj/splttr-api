package com.splttr.applicationservice.expense;

import com.splttr.applicationapi.expense.ExpenseInfoTO;
import com.splttr.applicationservice.person.PersonTOMapper;
import com.splttr.domain.expense.Expense;
import com.splttr.domain.expense.ExpenseRepository;
import com.splttr.domain.person.Person;
import com.splttr.domain.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Component
public class ExpenseInfoTOMapper {

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonTOMapper personTOMapper;

    private ExpenseInfoTOMapper() {
    }

    public List<ExpenseInfoTO> mapToExpenseInfoTOs(UUID tripId) {
        List<ExpenseInfoTO> expenseInfoTOS = expenseRepository.findByTripId(tripId).stream()
                .map(this::mapToExpenseInfoTO)
                .sorted(comparing(ExpenseInfoTO::getDate).reversed())
                .collect(toList());

        return expenseInfoTOS;
    }

    private ExpenseInfoTO mapToExpenseInfoTO(Expense expense) {
        Person payer = personRepository.findByPersonId(expense.getPayerId())
                .orElseThrow(() -> new IllegalStateException("Person should be present for id: " + expense.getPayerId()));

        return new ExpenseInfoTO(
                personTOMapper.mapToPersonTO(payer),
                expense.getDescription(),
                expense.getDate(),
                expense.getAmount()
        );
    }

}
