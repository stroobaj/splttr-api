package com.splttr.applicationservice.expense;

import com.splttr.applicationapi.expense.ExpenseAllocationTO;
import com.splttr.applicationapi.expense.ExpenseInfoTO;
import com.splttr.applicationservice.person.PersonTOMapper;
import com.splttr.domain.expense.ExpenseAllocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Component
public class ExpenseAllocationTOMapper {

    @Autowired
    private PersonTOMapper personTOMapper;

    private ExpenseAllocationTOMapper() {
    }

    List<ExpenseAllocationTO> mapToExpenseAllocationTOs(Set<ExpenseAllocation> expenseAllocations) {
        return expenseAllocations.stream()
                .map(expenseAllocation ->
                        new ExpenseAllocationTO(personTOMapper.mapToPersonTO(expenseAllocation.getPersonId()), expenseAllocation.getAmount()))
                .collect(toList());
    }
}
