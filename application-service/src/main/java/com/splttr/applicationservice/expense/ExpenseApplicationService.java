package com.splttr.applicationservice.expense;

import com.splttr.applicationapi.expense.CreateExpenseTO;
import com.splttr.applicationapi.expense.ExtendedExpenseInfoTO;
import com.splttr.applicationservice.person.PersonTOMapper;
import com.splttr.domain.expense.ExpenseDomainService;
import com.splttr.domain.expense.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Component
@Transactional
public class ExpenseApplicationService {

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private ExpenseDomainService expenseDomainService;

    @Autowired
    private PersonTOMapper personTOMapper;

    @Autowired
    private ExpenseAllocationTOMapper expenseAllocationTOMapper;

    public void createExpense(CreateExpenseTO createExpenseTO) {
        UUID tripId = createExpenseTO.getTripId();
        UUID payerId = createExpenseTO.getPayerId();

        expenseDomainService.createExpense(
                tripId,
                payerId,
                createExpenseTO.getAmount(),
                createExpenseTO.getDate(),
                createExpenseTO.getDescription(),
                createExpenseTO.getPartakers());
    }

    public List<ExtendedExpenseInfoTO> getExtendExpensesByByTripId(UUID tripId) {
        return expenseRepository.findByTripId(tripId).stream()
                .map(expense -> new ExtendedExpenseInfoTO(
                        personTOMapper.mapToPersonTO(expense.getPayerId()),
                        expense.getDescription(),
                        expense.getDate(),
                        expense.getAmount(),
                        expenseAllocationTOMapper.mapToExpenseAllocationTOs(expense.getExpenseAllocations())))
                .sorted(comparing(ExtendedExpenseInfoTO::getDate).reversed())
                .collect(toList());
    }
}
