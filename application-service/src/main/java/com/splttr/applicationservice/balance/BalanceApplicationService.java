package com.splttr.applicationservice.balance;

import com.splttr.applicationapi.balance.PersonBalanceTO;
import com.splttr.applicationapi.balance.UserBalanceTO;
import com.splttr.applicationservice.person.UserBalanceTOFactory;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Component
@Transactional
public class BalanceApplicationService {

    private PersonBalanceTOMapper personBalanceTOMapper;
    private UserBalanceTOFactory userBalanceTOFactory;

    public BalanceApplicationService(PersonBalanceTOMapper personBalanceTOMapper,
                                     UserBalanceTOFactory userBalanceTOFactory) {
        this.personBalanceTOMapper = personBalanceTOMapper;
        this.userBalanceTOFactory = userBalanceTOFactory;
    }

    public List<PersonBalanceTO> getTripBalances(UUID tripId) {
        return personBalanceTOMapper.mapToPersonBalanceTOs(tripId);
    }

    public List<UserBalanceTO> getUserBalances(UUID principal) {
        return userBalanceTOFactory.mapToUserBalanceTOs(principal);
    }

    public List<UserBalanceTO> getLastEditedUserBalances(UUID principal) {
        return userBalanceTOFactory.mapToLastEditedUserBalanceTOs(principal);
    }

}
