package com.splttr.applicationservice.balance;

import com.splttr.applicationapi.balance.PersonBalanceTO;
import com.splttr.applicationapi.person.PersonTO;
import com.splttr.domain.balance.Balance;
import com.splttr.domain.balance.BalanceRepository;
import com.splttr.domain.person.Person;
import com.splttr.domain.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Component
public class PersonBalanceTOMapper {

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private PersonRepository personRepository;

    private PersonBalanceTOMapper() {
    }

    public List<PersonBalanceTO> mapToPersonBalanceTOs(UUID tripId) {
        List<Balance> tripBalances = balanceRepository.findByTripId(tripId);

        return tripBalances.stream()
                .map(this::mapToPersonBalanceTO)
                .collect(toList());
    }

    private PersonBalanceTO mapToPersonBalanceTO(Balance balance) {
        UUID personId = balance.getPersonId();

        Person person = personRepository.findByPersonId(personId)
                .orElseThrow(() -> new IllegalStateException("Person should be present for id: " + personId));
        PersonTO personTO = new PersonTO(person.getPersonId(), person.getFullName(), person.getEmail());

        return new PersonBalanceTO(
                personTO,
                balance.getBalance().toString(),
                balance.getAdvance().toString(),
                balance.getBorrowed().toString()
        );
    }

}
