package com.splttr.applicationservice.person;

import com.splttr.applicationapi.person.PersonTO;
import com.splttr.domain.person.Person;
import com.splttr.domain.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Component
public class PersonTOMapper {

    @Autowired
    private PersonRepository personRepository;

    private PersonTOMapper() {
    }

    public PersonTO mapToPersonTO(Person person) {
        return new PersonTO(person.getPersonId(), person.getFullName(), person.getEmail());
    }

    public PersonTO mapToPersonTO(UUID personId) {
        return personRepository.findByPersonId(personId)
                .map(person -> new PersonTO(person.getPersonId(), person.getFullName(), person.getEmail()))
                .orElseThrow(() -> new IllegalStateException("Person should be present for id: " + personId));
    }

    public List<PersonTO> mapToPersonTOs(List<UUID> persons) {
        return persons.stream()
                .map(personId -> personRepository.findByPersonId(personId)
                        .orElseThrow(() -> new IllegalStateException("Person should be present for id: " + personId)))
                .map(this::mapToPersonTO)
                .collect(toList());
    }

}
