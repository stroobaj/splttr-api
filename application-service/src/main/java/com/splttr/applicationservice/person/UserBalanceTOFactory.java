package com.splttr.applicationservice.person;

import com.splttr.applicationapi.balance.UserBalanceTO;
import com.splttr.applicationservice.trip.TripApplicationService;
import com.splttr.domain.balance.Balance;
import com.splttr.domain.balance.BalanceRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Component
public class UserBalanceTOFactory {

    private BalanceRepository balanceRepository;
    private TripApplicationService tripApplicationService;

    public UserBalanceTOFactory(BalanceRepository balanceRepository, TripApplicationService tripApplicationService) {
        this.balanceRepository = balanceRepository;
        this.tripApplicationService = tripApplicationService;
    }

    public List<UserBalanceTO> mapToUserBalanceTOs(UUID principal) {
        return balanceRepository.findByPersonId(principal).stream()
                .map(balance -> mapToUserBalanceTO(balance, principal))
                .collect(toList());
    }

    public List<UserBalanceTO> mapToLastEditedUserBalanceTOs(UUID principal) {
        return balanceRepository.findByPersonId(principal).stream()
                .map(balance -> mapToUserBalanceTO(balance, principal))
                .sorted(comparing(UserBalanceTO::getLastEditDateTime).reversed())
                .limit(3)
                .collect(toList());
    }

    private UserBalanceTO mapToUserBalanceTO(Balance balance, UUID principal) {
        return new UserBalanceTO(
                balance.getTripId(),
                getTripName(balance.getTripId(), principal),
                balance.getBalance().toString(),
                balance.getLastEditDateTime());
    }

    private String getTripName(UUID tripId, UUID principal) {
        return tripApplicationService.getTripName(tripId, principal);
    }

}
