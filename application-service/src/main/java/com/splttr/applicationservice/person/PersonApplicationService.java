package com.splttr.applicationservice.person;

import com.splttr.applicationapi.person.PersonInfoTO;
import com.splttr.applicationapi.person.PersonTO;
import com.splttr.domain.person.Person;
import com.splttr.domain.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

import static com.splttr.domain.person.Person.newPersonBuilder;
import static java.util.UUID.fromString;
import static java.util.stream.Collectors.toList;

@Component
@Transactional
public class PersonApplicationService {

    @Autowired
    private PersonRepository personRepository;

    public List<PersonTO> getAllUserPersons(UUID principal) {
        return personRepository.findAllUserPersons(principal)
                .stream()
                .map((this::mapToPersonTO))
                .collect(toList());
    }

    private PersonTO mapToPersonTO(Person person) {
        return new PersonTO(person.getPersonId(), person.getFullName(), person.getEmail());
    }

    public void createPerson(PersonInfoTO personInfoTO, UUID principal) {
        UUID personId = UUID.randomUUID();
        Person person = newPersonBuilder()
                .withPersonId(personId)
                .withPrincipal(principal)
                .withFirstName(personInfoTO.getFirstName())
                .withLastName(personInfoTO.getLastName())
                .withEmail(personInfoTO.getEmail())
                .build();

        personRepository.save(person);
    }

    public void editPerson(String personId, PersonInfoTO personInfoTO, UUID principal) {
        // TODO: update possibly changed user data
        personRepository.save(newPersonBuilder()
                .withPrincipal(principal)
                .withPersonId(fromString(personId))
                .withFirstName(personInfoTO.getFirstName())
                .withLastName(personInfoTO.getLastName())
                .withEmail(personInfoTO.getEmail())
                .build());
    }

}
