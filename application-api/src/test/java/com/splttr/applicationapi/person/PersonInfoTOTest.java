package com.splttr.applicationapi.person;

import com.splttr.commons.test.ValidatorRule;
import org.junit.Rule;
import org.junit.Test;

import static com.splttr.commons.test.ValidatorRule.validator;

public class PersonInfoTOTest {

    @Rule
    public ValidatorRule validator = validator();

    @Test
    public void firstNameNull_invalid() {
        PersonInfoTO personInfoTO = new PersonInfoTO(null, "lastname", "test@gmail.com");

        validator.assertNotValid(personInfoTO);
        validator.assertMessages(personInfoTO, "splttr.FIRST_NAME_REQUIRED");
    }

    @Test
    public void lastNameNull_invalid() {
        PersonInfoTO personInfoTO = new PersonInfoTO("firstname", null, "test@gmail.com");

        validator.assertNotValid(personInfoTO);
        validator.assertMessages(personInfoTO, "splttr.LAST_NAME_REQUIRED");
    }

    @Test
    public void emailNull_valid() {
        PersonInfoTO personInfoTO = new PersonInfoTO("firstname", "lastname", null);

        validator.assertValid(personInfoTO);
    }

    @Test
    public void allFieldsFilledIn_valid() {
        PersonInfoTO personInfoTO = new PersonInfoTO("firstname", "lastname", "test@gmail.com");

        validator.assertValid(personInfoTO);
    }

}
