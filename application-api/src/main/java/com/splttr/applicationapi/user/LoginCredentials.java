package com.splttr.applicationapi.user;

public class LoginCredentials {

    private String username;
    private String password;

    private LoginCredentials() {
    }

    public LoginCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
