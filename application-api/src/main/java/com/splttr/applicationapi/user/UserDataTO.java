package com.splttr.applicationapi.user;

public class UserDataTO {

    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;

    private UserDataTO() {
    }

    public UserDataTO(String firstName, String lastName, String username, String password, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

}
