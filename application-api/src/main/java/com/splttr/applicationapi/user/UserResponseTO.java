package com.splttr.applicationapi.user;

import com.splttr.domain.user.Role;

import java.util.List;
import java.util.UUID;

public class UserResponseTO {

    private UUID userId;
    private String username;
    private String email;
    private List<Role> roles;

    public UserResponseTO(UUID userId, String username, String email, List<Role> roles) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public List<Role> getRoles() {
        return roles;
    }
}
