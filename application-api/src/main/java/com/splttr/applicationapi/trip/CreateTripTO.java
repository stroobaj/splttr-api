package com.splttr.applicationapi.trip;

import java.util.List;
import java.util.UUID;

public class CreateTripTO {

    private String name;
    private List<UUID> persons;

    private CreateTripTO() {
    }

    public CreateTripTO(String name, List<UUID> persons) {
        this.name = name;
        this.persons = persons;
    }

    public String getName() {
        return name;
    }

    public List<UUID> getPersons() {
        return persons;
    }

}
