package com.splttr.applicationapi.trip;

import com.splttr.applicationapi.balance.PersonBalanceTO;
import com.splttr.applicationapi.expense.ExpenseInfoTO;
import com.splttr.applicationapi.person.PersonTO;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class TripOverviewTO {

    private UUID tripId;
    private String name;
    private LocalDateTime creationDateTime;
    private LocalDateTime lastEditDateTime;
    private List<PersonTO> persons;
    private List<ExpenseInfoTO> expenses;
    private List<PersonBalanceTO> balances;

    private TripOverviewTO() {
    }

    public TripOverviewTO(UUID tripId,
                          String name,
                          LocalDateTime creationDateTime,
                          LocalDateTime lastEditDateTime,
                          List<PersonTO> persons,
                          List<PersonBalanceTO> balances,
                          List<ExpenseInfoTO> expenses) {
        this.tripId = tripId;
        this.name = name;
        this.creationDateTime = creationDateTime;
        this.lastEditDateTime = lastEditDateTime;
        this.persons = persons;
        this.balances = balances;
        this.expenses = expenses;
    }

    public UUID getTripId() {
        return tripId;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public LocalDateTime getLastEditDateTime() {
        return lastEditDateTime;
    }

    public List<PersonTO> getPersons() {
        return persons;
    }

    public List<PersonBalanceTO> getBalances() {
        return balances;
    }

    public List<ExpenseInfoTO> getExpenses() {
        return expenses;
    }
}
