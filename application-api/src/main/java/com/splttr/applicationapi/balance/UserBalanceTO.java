package com.splttr.applicationapi.balance;

import com.splttr.commons.equals.EqualByStateObject;

import java.time.LocalDateTime;
import java.util.UUID;

public class UserBalanceTO extends EqualByStateObject {

    private UUID tripId;
    private String tripName;
    private String balance;
    private LocalDateTime lastEditDateTime;

    private UserBalanceTO() {
    }

    public UserBalanceTO(UUID tripId, String tripName, String balance, LocalDateTime lastEditDateTime) {
        this.tripId = tripId;
        this.tripName = tripName;
        this.balance = balance;
        this.lastEditDateTime = lastEditDateTime;
    }

    public UUID getTripId() {
        return tripId;
    }

    public String getTripName() {
        return tripName;
    }

    public String getBalance() {
        return balance;
    }

    public LocalDateTime getLastEditDateTime() {
        return lastEditDateTime;
    }
}
