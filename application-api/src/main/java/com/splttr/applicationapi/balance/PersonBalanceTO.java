package com.splttr.applicationapi.balance;

import com.splttr.applicationapi.person.PersonTO;
import com.splttr.commons.equals.EqualByStateObject;

public class PersonBalanceTO extends EqualByStateObject {

    private PersonTO person;
    private String balance;
    private String advance;
    private String borrowed;

    private PersonBalanceTO() {
    }

    public PersonBalanceTO(PersonTO person, String balance, String advance, String borrowed) {
        this.person = person;
        this.balance = balance;
        this.advance = advance;
        this.borrowed = borrowed;
    }

    public PersonTO getPerson() {
        return person;
    }

    public String getBalance() {
        return balance;
    }

    public String getAdvance() {
        return advance;
    }

    public String getBorrowed() {
        return borrowed;
    }
}
