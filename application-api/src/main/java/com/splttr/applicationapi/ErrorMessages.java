package com.splttr.applicationapi;

public interface ErrorMessages {

    String PREFIX = "splttr.";

    interface Person {
        String FIRST_NAME_REQUIRED = PREFIX + "FIRST_NAME_REQUIRED";
        String LAST_NAME_REQUIRED = PREFIX + "LAST_NAME_REQUIRED";
    }
}
