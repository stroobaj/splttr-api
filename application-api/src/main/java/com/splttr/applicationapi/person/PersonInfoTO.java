package com.splttr.applicationapi.person;

import com.splttr.commons.equals.EqualByStateObject;

import javax.validation.constraints.NotNull;

import static com.splttr.applicationapi.ErrorMessages.Person.FIRST_NAME_REQUIRED;
import static com.splttr.applicationapi.ErrorMessages.Person.LAST_NAME_REQUIRED;

public class PersonInfoTO extends EqualByStateObject {

    @NotNull(message = FIRST_NAME_REQUIRED)
    private String firstName;
    @NotNull(message = LAST_NAME_REQUIRED)
    private String lastName;
    private String email;

    private PersonInfoTO() {
    }

    public PersonInfoTO(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

}
