package com.splttr.applicationapi.person;

import com.splttr.commons.equals.EqualByStateObject;
import com.splttr.domain.person.FullName;

import java.util.UUID;

public class PersonTO extends EqualByStateObject {

    private UUID personId;
    private FullName fullName;
    private String email;

    private PersonTO() {
    }

    public PersonTO(UUID personId, FullName fullName, String email) {
        this.personId = personId;
        this.fullName = fullName;
        this.email = email;
    }

    public UUID getPersonId() {
        return personId;
    }

    public FullName getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

}
