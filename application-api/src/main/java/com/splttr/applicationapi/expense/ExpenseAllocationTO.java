package com.splttr.applicationapi.expense;

import com.splttr.applicationapi.person.PersonTO;

import java.math.BigDecimal;

public class ExpenseAllocationTO {

    private PersonTO person;
    private BigDecimal amount;

    private ExpenseAllocationTO() {
    }

    public ExpenseAllocationTO(PersonTO person, BigDecimal amount) {
        this.person = person;
        this.amount = amount;
    }

    public PersonTO getPerson() {
        return person;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
