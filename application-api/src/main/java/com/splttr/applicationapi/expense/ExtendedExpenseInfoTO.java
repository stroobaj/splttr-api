package com.splttr.applicationapi.expense;

import com.splttr.applicationapi.person.PersonTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class ExtendedExpenseInfoTO {

    private PersonTO payer;
    private String description;
    private LocalDate date;
    private BigDecimal amount;
    private List<ExpenseAllocationTO> expenseAllocations;

    private ExtendedExpenseInfoTO() {
    }

    public ExtendedExpenseInfoTO(PersonTO payer, String description, LocalDate date, BigDecimal amount, List<ExpenseAllocationTO> expenseAllocations) {
        this.payer = payer;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.expenseAllocations = expenseAllocations;
    }

    public PersonTO getPayer() {
        return payer;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public List<ExpenseAllocationTO> getExpenseAllocations() {
        return expenseAllocations;
    }
}
