package com.splttr.applicationapi.expense;

import com.splttr.applicationapi.person.PersonTO;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExpenseInfoTO {

    private PersonTO payer;
    private String description;
    private LocalDate date;
    private BigDecimal amount;

    private ExpenseInfoTO() {
    }

    public ExpenseInfoTO(PersonTO payer, String description, LocalDate date, BigDecimal amount) {
        this.payer = payer;
        this.description = description;
        this.date = date;
        this.amount = amount;
    }

    public PersonTO getPayer() {
        return payer;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
