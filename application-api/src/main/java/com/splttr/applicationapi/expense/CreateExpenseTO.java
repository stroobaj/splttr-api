package com.splttr.applicationapi.expense;

import com.splttr.commons.equals.EqualByStateObject;

import java.util.List;
import java.util.UUID;

public class CreateExpenseTO extends EqualByStateObject {

    private UUID tripId;
    private UUID payerId;
    private String description;
    private String date;
    private String amount;
    private List<UUID> partakers;

    private CreateExpenseTO() {
    }

    public CreateExpenseTO(UUID tripId, UUID payerId, String description, String date, String amount, List<UUID> partakers) {
        this.tripId = tripId;
        this.payerId = payerId;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.partakers = partakers;
    }

    public UUID getTripId() {
        return tripId;
    }

    public UUID getPayerId() {
        return payerId;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getAmount() {
        return amount;
    }

    public List<UUID> getPartakers() {
        return partakers;
    }
}
