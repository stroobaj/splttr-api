package com.splttr.commons.test;

import org.junit.rules.ExternalResource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

public class ValidatorRule extends ExternalResource {

    private static Validator validator;

    public static ValidatorRule validator() {
        return new ValidatorRule();
    }

    @Override
    protected void before() throws Throwable {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    public void assertValid(Object object) {
        Set<ConstraintViolation<Object>> violations = validator.validate(object);
        assertThat(violations).isEmpty();
    }

    public void assertNotValid(Object object) {
        Set<ConstraintViolation<Object>> violations = validator.validate(object);
        assertThat(violations).isNotEmpty();
    }

    public void assertMessages(Object object, String... expectedMessages) {
        Set<ConstraintViolation<Object>> violations = validator.validate(object);

        Set<String> actualMessages = violations.stream().map(ConstraintViolation::getMessage).collect(toSet());

        assertThat(actualMessages).containsExactlyInAnyOrder(expectedMessages);
    }
}
