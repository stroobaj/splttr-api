package com.splttr.commons.equals;


import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Optional;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;


/**
 * Base class to use for having reflectionEquals(), reflectionHashCode() & ReflectionToString
 */
public abstract class EqualByStateObject {

    @Override
    public boolean equals(Object other) {
        return Optional.ofNullable(other)
                .filter(o -> this.getClass().equals(o.getClass()))
                .map(o -> EqualsBuilder.reflectionEquals(this, other))
                .orElse(false);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, SHORT_PREFIX_STYLE);
    }
}

