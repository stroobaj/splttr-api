package com.splttr.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
public abstract class EntityId implements Serializable {

    @Column(name = "principal")
    private UUID principal;

    @Column(name = "id")
    private UUID id;

    protected EntityId() {
    }

    protected EntityId(UUID principal, UUID id) {
        this.principal = principal;
        this.id = id;
    }

    public UUID getPrincipal() {
        return principal;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public String toString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
