package com.splttr.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class HttpInterceptorForPrincipalServiceConfiguration implements WebMvcConfigurer {

    private final PrincipalService principalService;

    public HttpInterceptorForPrincipalServiceConfiguration(PrincipalService principalService) {
        this.principalService = principalService;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HttpInterceptorForPrincipalService(principalService)).excludePathPatterns("/auth/**");
    }
}
