package com.splttr.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpInterceptorForPrincipalService extends HandlerInterceptorAdapter {

    private final PrincipalService principalService;
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpInterceptorForPrincipalService.class);

    public HttpInterceptorForPrincipalService(PrincipalService principalService) {
        this.principalService = principalService;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) {
        if (isUserLogged()) {
            principalService.clearPrincipal((Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        } else {
            principalService.clearAnonymousPrincipal();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        // This space is intentionally left blank
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        LOGGER.info("=============== preHandle ===============");

        try {
            // TODO: revert log level
            LOGGER.info("Principal: " + SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        } catch (NullPointerException ex) {
            // TODO: revert log level
            LOGGER.info("No principal found");
            return false;
        }

        if (isUserLogged()) {
            Principal principal = (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            principalService.setPrincipal(principal);
        }

        LOGGER.info("=============== preHandle ===============");
        return true;
    }

    private static boolean isUserLogged() {
        try {
            return !SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser");
        } catch (Exception e) {
            return false;
        }
    }
}
