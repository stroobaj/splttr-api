package com.splttr.security;


import org.springframework.stereotype.Component;

import java.util.UUID;

import static java.lang.String.format;

@Component
public class PrincipalService {

    private static ThreadLocal<Principal> principal = new ThreadLocal<>();

    public void setPrincipal(Principal principal) {
        if (isPrincipalEmpty()) {
            PrincipalService.principal.set(principal);
        } else {
            throw new IllegalArgumentException(format("Cannot set Principal %s, Principal already stored as %s", principal, PrincipalService.principal.get()));
        }
    }

    public Principal getPrincipal() {
        return PrincipalService.principal.get();
    }

    public UUID getPrincipalId() {
        return PrincipalService.principal.get().getUserId();
    }

    public void clearPrincipal(Principal principal) {
        if (PrincipalService.principal.get().equals(principal)) {
            PrincipalService.principal.remove();
        } else {
            throw new IllegalArgumentException(format("Cannot clear principal %s, stored value is %s", principal, PrincipalService.principal.get()));
        }
    }

    public void clearAnonymousPrincipal() {
        PrincipalService.principal.remove();
    }

    private boolean isPrincipalEmpty() {
        return principal.get() == null;
    }

}
